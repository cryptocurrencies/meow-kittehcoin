# ---- Base Node ---- #
FROM ubuntu:14.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu trusty main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev git-core libboost-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config sudo && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ---- #
FROM base as build
RUN git clone https://github.com/kittehcoin/kittehcoin.git /opt/kittehcoin 
RUN cd /opt/kittehcoin/src && \
    make -j2 -f makefile.unix

# ---- Release ---- #
FROM ubuntu:14.04 as release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu trusty main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y  libboost-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r kittehcoin && useradd -r -m -g kittehcoin kittehcoin
RUN mkdir /data
COPY --from=build /opt/kittehcoin/src/kittehcoind /usr/local/bin/
RUN chown kittehcoin:kittehcoin /data
USER kittehcoin
VOLUME /data
EXPOSE 22566 22565
CMD ["/usr/local/bin/kittehcoind", "datadir=/data", "-conf=/data/kittehcoin.conf", "-server", "-txindex", "-printtoconsole"]